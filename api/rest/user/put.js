var mongodb = require("mongodb");
var Q = require("q");

module.exports = function(id, user) {
  var deferred = Q.defer();
  mongodb.MongoClient.connect("mongodb://test:test123@troup.mongohq.com:10049/ratelus", function(err, db) {
    if (err !== null) {
      console.error(err);
    }

    user._id = id;
    
    console.info(user);

    var userCollection = db.collection("user");

    userCollection.save(user, function(err, record) {
      var update = (record === 1) ? true : false;
      deferred.resolve(update);
      if (err !== undefined) {
        deferred.reject(err);
      }
    });
  });
  return deferred.promise;
};
