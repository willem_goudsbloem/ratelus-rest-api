var http = require('http');
var url = require("url");
var Q = require("q");
var express = require("express");
var cors = require("express-cors");
var userPut = require("./api/rest/user/put");

var HTTP_PORT = process.env.PORT || 8082;

var app = express();

app.use(cors({
  allowedOrigins: [
    'http://localhost:8383', 'ratelus-web-client.herokuapp.com'
  ]
}));

app.use(express.json());

var error = function(err) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });
  res.end('Server Error :(');
};

var unkown = function(err) {
  res.writeHead(404, {
    'Content-Type': 'text/plain'
  });
  res.end('Resource Unkown :(');
};

var http200 = function(val) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });
  res.end(val);
  return;
};

var http200 = function() {
  res.writeHead(200);
};

var http201 = function() {
  res.writeHead(201);
};

var http500 = function(err) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });
  res.end('Server Error :(');
};


app.put("/api/rest/user/:id", function(req, res) {
  // console.info(req.body);
  userPut(req.params.id, req.body).then(function(update) {
    if (update)
      httpp201();
    else
      http200();
  }, error);
});

app.listen(HTTP_PORT);

app.on('error', function(e) {
  if (e.code === 'EADDRINUSE') {
    console.error("Server did not start port '" + HTTP_PORT + "' already in use");
  }
});
